﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogApp.Data.Abstract;
using BlogApp.Entity;
using Microsoft.AspNetCore.Mvc;

namespace BlogApp.WebUI.Controllers
{
    public class CategoryController : Controller
    {
        private ICategoryRepository repository;
        public CategoryController(ICategoryRepository _repo)
        {
            repository = _repo;
        }
        public IActionResult Index()
        {
            return View();
        }
        //List
        public IActionResult List()
        {
            return View(repository.GetAll());
        }

        #region Create and Edit (Bunlar İçin Başka Metot Kullanıldı)
        //Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Category entity)
        {
            if (ModelState.IsValid)
            {
                repository.AddCategory(entity);
                return RedirectToAction("List");
            }
            return View(entity);
        }


        //Edit
        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(repository.GetById(id));
        }

        [HttpPost]
        public IActionResult Edit(Category entity)
        {
            if (ModelState.IsValid)
            {
                repository.UpdateCategory(entity);
                TempData["Message"] = $"{entity.Name} Güncellendi.";
                return RedirectToAction("List");
            }
            return View(entity);
        }
        #endregion

        //Add or Update
        [HttpGet]
        public IActionResult AddOrUpdate(int? id)
        {
            if (id == null)
            {
                return View(new Category());
            }
            else
            {
                return View(repository.GetById((int)id));
            }
        }

        [HttpPost]
        public IActionResult AddOrUpdate(Category entity)
        {
            if (ModelState.IsValid)
            {
                repository.AddOrUpdate(entity);
                TempData["Message"] = $"{entity.Name} Kayıt Edildi.";
                return RedirectToAction("List");
            }
            return View(entity);
        }

        //Delete
        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(repository.GetById(id));
        }

        [HttpPost,ActionName("Delete")]
        public IActionResult DeleteConfirmed(int categoryId)
        {
            repository.DeleteCategory(categoryId);
            TempData["Message"] = $"{categoryId} Numaralı ID'ye Sahip Kayıt Silindi.";
            return RedirectToAction("List");
        }
    }
}