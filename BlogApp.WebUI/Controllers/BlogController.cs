﻿using BlogApp.Data.Abstract;
using BlogApp.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.WebUI.Controllers
{
    public class BlogController : Controller
    {
        private IBlogRepository _blogRepository;
        private ICategoryRepository _categoryRepository;

        public BlogController(IBlogRepository blogRepo, ICategoryRepository categoryRepo)
        {
            _blogRepository = blogRepo;
            _categoryRepository = categoryRepo;
        }

        //Index
        public IActionResult Index(int? id, string q)
        {
            var query = _blogRepository.GetAll().Where(x => x.IsApproved);
            if (id != null)
            {
                query = query.Where(x => x.CategoryId == id);
            }
            if (!string.IsNullOrEmpty(q))
            {
                //query = query.Where(x => x.Title.Contains(q) || x.Description.Contains(q) || x.Body.Contains(q));
                query = query.Where(x => EF.Functions.Like(x.Title, "%" + q + "%") || EF.Functions.Like(x.Description, "%" + q + "%") || EF.Functions.Like(x.Title, "%" + q + "%"));
            }
            return View(query.OrderByDescending(x => x.Date));
        }

        //Details
        public IActionResult Details(int id)
        {
            return View(_blogRepository.GetById(id));
        }
        //List
        public IActionResult List()
        {
            return View(_blogRepository.GetAll());
        }

        //Create
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
            return View(new Blog());
        }
        [HttpPost]
        public async Task<IActionResult> Create(Blog entity, IFormFile file)
        {
            entity.Date = DateTime.Now;
            if (ModelState.IsValid)
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                entity.Image = file.FileName;
                _blogRepository.AddBlog(entity);
                TempData["Message"] = $"{entity.Title} Kayıt Edildi.";
                return RedirectToAction("List");
            }
            ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
            return View(entity);
        }

        //Edit
        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
            return View(_blogRepository.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Blog entity, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                    entity.Image = file.FileName;
                }
                _blogRepository.UpdateBlog(entity);
                TempData["Message"] = $"{entity.Title} Güncellendi.";
                return RedirectToAction("List");
            }
            ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
            return View(entity);
        }

        //Delete
        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(_blogRepository.GetById(id));
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int blogId)
        {
            _blogRepository.DeleteBlog(blogId);
            TempData["Message"] = $"{blogId} numaralı ID'ye Sahip Kayıt Silindi.";
            return RedirectToAction("List");
        }


        #region Create (Çeşitli Denemelerden Dolayı Fazla Fazla Fonksiyon Var)
        ////Create
        //[HttpGet]
        //public IActionResult Create()
        //{
        //    ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
        //    return View(new Blog());

        //}

        //[HttpPost]
        //public IActionResult Create(Blog entity)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _blogRepository.AddOrUpdate(entity);
        //        TempData["Message"] = $"{entity.Title} Kayıt Edildi.";
        //        return RedirectToAction("List");
        //    }
        //    ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
        //    return View(entity);
        //}

        ////Edit
        //[HttpGet]
        //public IActionResult Edit(int id)
        //{
        //    ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");

        //    return View(_blogRepository.GetById(id));
        //}

        //[HttpPost]
        //public async Task<IActionResult> Edit(Blog entity, IFormFile file)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        if (file != null)
        //        {
        //            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
        //            using (var stream = new FileStream(path, FileMode.Create))
        //            {
        //                await file.CopyToAsync(stream);
        //            }

        //            entity.Image = file.FileName;
        //        }

        //        _blogRepository.AddOrUpdate(entity);
        //        TempData["Message"] = $"{entity.Title} Kayıt Edildi.";
        //        return RedirectToAction("List");
        //    }
        //    ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
        //    return View(entity);
        //}


        #endregion

        #region Add or Update (Tek sayfa üzerinden yapmak için kullanılan method)

        ////Add or Update
        //[HttpGet]
        //public IActionResult AddOrUpdate(int? id)
        //{
        //    ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
        //    if (id == null)
        //    {
        //        //yeni kayıt
        //        return View(new Blog());
        //    }
        //    else
        //    {
        //        //güncelleme
        //        return View(_blogRepository.GetById((int)id));
        //    }
        //}

        //[HttpPost]
        //public IActionResult AddOrUpdate(Blog entity)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _blogRepository.AddOrUpdate(entity);
        //        TempData["Message"] = $"{entity.Title} Kayıt Edildi.";
        //        return RedirectToAction("List");
        //    }
        //    ViewBag.Categories = new SelectList(_categoryRepository.GetAll(), "CategoryId", "Name");
        //    return View(entity);
        //}

        #endregion




    }
}